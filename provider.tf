terraform {
  required_version = ">= 1.2.5"
  required_providers {
    newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 3.27.7"
    }
  }
}

provider "newrelic" {
  api_key = var.newrelic_api_key
}
