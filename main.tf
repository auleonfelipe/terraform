locals {
  app_settings_map = { for app in var.app_settings : app.app_name => app}
}

data "newrelic_entity" "applications" {
  for_each = local.app_settings_map
  name   = each.value.app_name
  domain = "APM"
  type   = "APPLICATION"
}

resource "newrelic_alert_policy" "red_policy" {
  for_each = local.app_settings_map
  name = each.value.app_name
  incident_preference = "PER_CONDITION"
}

resource "newrelic_nrql_alert_condition" "error_percentage" {
  for_each = local.app_settings_map
  policy_id                      = newrelic_alert_policy.red_policy[each.key].id
  type                           = "static"
  name                           = "${each.value.app_name} - Taxa de erro"
  description                    = "Gera um alerta quando ocorre aumento na taxa de erro"
  runbook_url                    = each.value.runbook_url
  enabled                        = each.value.error_alert_enabled
  violation_time_limit_seconds   = 2592000
  aggregation_window             = 60

  nrql {
    query = "SELECT (count(apm.service.error.count) / count(apm.service.transaction.duration)) * 100 AS 'Error %' FROM Metric WHERE entity.guid = '${data.newrelic_entity.applications[each.key].guid}'"
  }

  critical {
    operator              = "above"
    threshold             = each.value.error_threshold
    threshold_duration    = each.value.error_duration
    threshold_occurrences = "ALL"
  }
}

resource "newrelic_nrql_alert_condition" "response_time" {
  for_each = local.app_settings_map
  policy_id = newrelic_alert_policy.red_policy[each.key].id
  type = "baseline"
  name = "${each.value.app_name} - Tempo de resposta"
  description                    = "Gera um alerta quando o tempo de resposta está acima da baseline"
  runbook_url                    = each.value.runbook_url
  enabled                        = each.value.response_time_alert_enabled
  violation_time_limit_seconds   = 2592000
  aggregation_window             = 60
  baseline_direction = "upper_only"

  nrql {
    query = "SELECT average(apm.service.transaction.duration) * 1000 AS 'Response time (ms)' FROM Metric WHERE entity.guid = '${data.newrelic_entity.applications[each.key].guid}'"
  }

  warning {
    operator = "above"
    threshold = each.value.response_time_threshold
    threshold_duration = each.value.response_time_duration
    threshold_occurrences = "all"
  }
}

resource "newrelic_nrql_alert_condition" "throughput" {
  for_each = local.app_settings_map
  policy_id                      = newrelic_alert_policy.red_policy[each.key].id
  type                           = "static"
  name                           = "${each.value.app_name} - Throughput"
  description                    = "Gera um alerta se o throughput for 0"
  runbook_url                    = each.value.runbook_url
  enabled                        = each.value.throughput_alert_enabled
  violation_time_limit_seconds   = 2592000
  aggregation_window             = 300
  expiration_duration = 180
  open_violation_on_expiration = true

  nrql {
        query = "SELECT filter(count(newrelic.timeslice.value), WHERE metricTimesliceName = 'HttpDispatcher' or metricTimesliceName = 'OtherTransaction/all') OR 0 as 'Throughput' FROM Metric WHERE appId IN ('${data.newrelic_entity.applications[each.key].application_id}') AND metricTimesliceName IN ('HttpDispatcher', 'Agent/MetricsReported/count', 'OtherTransaction/all')"
  }

  critical {
    operator              = "below"
    threshold             = each.value.throughput_threshold
    threshold_duration    = each.value.throughput_duration
    threshold_occurrences = "ALL"
  }
}

resource "newrelic_entity_tags" "error" {
  for_each = local.app_settings_map
  guid = newrelic_nrql_alert_condition.error_percentage[each.key].entity_guid
  tag {
    key = "Priority"
    values = [each.value.error_alert_priority]
  }
  tag {
    key = "muted"
    values = [each.value.error_alert_muted]
  }
}

resource "newrelic_entity_tags" "response_time" {
  for_each = local.app_settings_map
  guid = newrelic_nrql_alert_condition.response_time[each.key].entity_guid
  tag {
    key = "Priority"
    values = [each.value.response_time_alert_priority]
  }
  tag {
    key = "muted"
    values = [each.value.response_time_alert_muted]
  }
}
resource "newrelic_entity_tags" "throughput" {
  for_each = local.app_settings_map
  guid = newrelic_nrql_alert_condition.throughput[each.key].entity_guid
  tag {
    key = "Priority"
    values = [each.value.throughput_alert_priority]
  }
  tag {
    key = "muted"
    values = [each.value.throughput_alert_muted]
  }
}