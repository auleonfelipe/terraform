variable "app_settings" {
  type = list(object({
    app_name = string
    error_threshold = optional(number, 1)
    error_duration = optional(number, 300)
    error_alert_priority = optional(string, "P2")
    error_alert_enabled = optional(bool, true)
    error_alert_muted = optional(bool, false)
    response_time_threshold = optional(number, 3)
    response_time_duration = optional(number, 900)
    response_time_alert_priority = optional(string, "P3")
    response_time_alert_enabled = optional(bool, true)
    response_time_alert_muted = optional(bool, false)
    throughput_threshold = optional(number, 1)
    throughput_duration = optional(number, 300)
    throughput_alert_priority = optional(string, "P2")
    throughput_alert_enabled = optional(bool, true)
    throughput_alert_muted = optional(bool, false)
    runbook_url = optional(string)
  }))
}