
# NewRelic | Alerta para aplicações

Esse módulo permite criar condições de alertas para as aplicações configuradas no NewRelic.

## O que é criado?

Para cada aplicação é criado uma **Police** com o nome da aplicação, contendo 3 condições de alerta, conforme abaixo:

| Sinal | Threshold | Prioridade |
|-------------|-------------|-------------|
| Taxa de erro  | Taxa de erro acima de 1% durante 5 minutos  | P2  |
| Tempo de resposta  | Desvio padrão de 3 pontos para cima durante 10 minutos  | P3  |
| Throughput  | Throughput zerado durante 3 minutos ou perda do sinal  | P2  |

## Como usar?

Passe para o módulo uma lista de objetos chamada app_settings, cada objeto poderá ter vários atributos, no entanto, o único obrigatório é o app_name, que deverá corresponder ao nome da aplicação no New Relic.

### Exemplo

```yaml
module "application_alerts" {
  source = "  source = "git::git@gitlab.mosaico.com.br:infra/ongoing/newrelic-application-alert.git?ref=1.1.1"
  
  squad_name = "Nome da Squad"

  app_settings = [
    {
      app_name        = "aplicação-1"
      error_threshold = 2
      error_alert_priority = "P3"
      error_alert_enabled = true
      runbook_url = "www.example.com.br"
    },
    {
      app_name = "ui-demo"
    }
  ]
  }
```
## Entradas

Segue abaixo uma lista de entradas disponíveis que permitem personalizar a criação das condições de alertas.

| Atributo | Requisito | Descrição |
|-------------|-------------|-------------|
| app_name | obrigatório | Nome da aplicação conforme configurada no NewRelic. |
| squad_name | obrigatório | Nome da Squad responsável pela aplicação. |
| error_threshold | opcional | O threshold para o alerta de erro, o padrão é 1%. |
| error_duration | opcional | A duração do erro em segundos para que o alerta seja disparado, o padrão é 300 (5 minutos).|
| error_alert_priority | opcional  | A prioridade do alerta de erro, o padrão é P2. |
| error_alert_enabled | opcional  | Define se a condição de alerta de erro deve está ativa ou não, o padrão é true. |
| error_alert_muted | opcional  | Define se a notificação do alerta de erro deve ser silenciado ou não, o padrão é false. |
| response_time_threshold | opcional  | O threshold para o desvido padrão do alerta de tempo de resposta, o padrão é 3. Quanto maior esse número, mais o tempo de resposta deve se distanciar do valor previsto para gerar um alerta. |
| response_time_duration | opcional  | A duração do desvio padrão em segundos para que o alerta seja disparado, o padrão é 900 (15 minutos).|
| response_time_alert_priority | opcional  | A prioridade do alerta de tempo de resposta, o padrão é P3. |
| response_time_alert_enabled | opcional  | Define se a condição de alerta de tempo de resposta deve está ativa ou não, o padrão é true. |
| response_time_alert_muted | opcional  | Define se a notificação do alerta de tempo de resposta deve ser silenciado ou não, o padrão é false. |
| throughput_threshold | opcional  | A quantidade minima necessária de throughput para que não seja gerado um alerta, o padrão é 1. |
| throughput_duration | opcional  | A duração em segundos que o throughput deve ficar abaixo do esperado para gerar o alerta, o padrão é 3 minutos. |
| throughput_alert_priority | opcional  | A prioridade do alerta de throughput, o padrão é P2. |
| throughput_alert_enabled | opcional  | Define se a condição de alerta de throughput deve está ativa ou não, o padrão é true. |
| throughput_alert_muted | opcional  | Define se a notificação do alerta de throughput deve ser silenciado ou não, o padrão é false. |
| runbook_url | opcional  | A url da documentação com o procedimento para tratar o alerta. |

## Prioridades

Para entender melhor o que significa cada prioridade, viste a página abaixo:

 - [Prioridades](https://zoomit2.jira.com/wiki/spaces/INFRA2/pages/2718138436/Prioridades)

